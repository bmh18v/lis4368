# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### LIS 4368 Requirements:

### Assignments:

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install JDK
	- Install Tomcat
	- Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorials (butbucketstationlocations and myteam quotes)
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Tomcat How-To Tutorial
	- Write Database Servlet Deploying Serlvet using @WebServlet
	- Provide Assessment Links and screenshots
	- Provide Screenshot of query results
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Create Entity Relationship Diagram (ERD)
	- Include 10 records per table
	- Forward Engineer Locally in MySQL Workbench
	

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create server side validation page
	- Test for validation
	- Skill sets 10-12
	

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Create data directory inside of crud
	- Create java files that will be in the crud directory called ConnectionPool, CustomerDB, and DBUtil
	- Compile new files using *.java and servlet-api.jar for CustomerServlet
	- Run A5 using Tomcat

### Projects:

 1. [P1 README.md](p1/README.md "My P1 README.MD file")
	- Modify tags in index.jsp
	- Change title, navigation links, and header tags to appropriate format
	- Add form controls to match attributes of customer entity
	- Use min/max jQuery validation
	- Use *regexp* to allow only certain characters for input boxes
	

2. [P2 README.md](p2/README.md "My P2 README.md file")
	- Complete JSP/Servlets web application
	- Utilize MVC Framework
	- Show CRUD functionality
	- Make sure program prevents SQL Injection
	- Compile Java files
	


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")













