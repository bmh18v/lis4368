> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Assignment 2 Requirements:

*Three Parts:*

1. Finish Tomcat How-To Tutorial
2. Write a Database Servlet Deploying Servlet using @Webservlet
3. Compile all servlet files

#### README.md file should include the following items:

* All Assessment links
* Only *one* screenshot of the query results from http://localhost:9999/hello/querybook.html 

Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/a2/index.jsp)


#### Assignment Screenshots:

*Screenshots of all assessment links & actual links:*

| Screenshot of **index.html** [Index.html Link](http://localhost:9999/hello/index.html): | Screenshot of **sayhello** [Sayhello Link](http://localhost:9999/hello/sayhello):
| :-|:-|:-
![Index.html](img/indexHtml.png "Index HTML") | ![Sayhello](img/sayHello.png "Sayhello")

| Screenshot of **querybook.html** [Querybook.html Link](http://localhost:9999/hello/querybook.html): | Screenshot of **sayhi** [Sayhi Link](http://localhost:9999/hello/sayhi):
| :-|:-|:-
![Querybook.html](img/queryBook.png "Querybook HTML") | ![Sayhi](img/sayhi.png "Sayhi")

| Screenshot of **selected querybook.html** [Selected Querybook.html Link](http://localhost:9999/hello/querybook.html): | Screenshot of **Query Results** [Query Results Link](http://localhost:9999/hello/query?author=Tan+Ah+Teck):
| :-|:-|:-
![Selected Querybook.html](img/selectedqueryBook.png "Selected Querybook HTML") | ![Query Resuls](img/queryResults.png "Query Results")

| Screenshot of *localhost A2 1*: | Screenshot of *localhost A2 2*:
| :-|:-|:-
![Local Host 1](img/localhost1.png "Local Host 1") | ![Sayhi](img/localhost2.png "Local Host 2")










