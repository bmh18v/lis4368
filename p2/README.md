> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Project 2 Requirements:

*Four Parts:*

1. Compile JSP/Servlet Web App
2. Utilize MVC Framework
3. Show CRUD functionality
4. Compile Java files
	

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry (customerform.jsp)
* Screenshot of Passed Validation (thanks.jsp)
* Screenshot of Display Data (customers.jsp)
* Screenshot of Modify Form (modify.jsp)
* Screenshot of Modified Data (customers.jsp)
* Screenshot of Delete Warning (customers.jsp)
* Screenshot of Associated Database Changes (Select, Insert, Update, Delete)

Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/p2/index.jsp)


#### Assignment Screenshots:

| Screenshot of Valid User Form Entry: | Screenshot of Passed Validation: 
| :-|:-|:-
![Valid Form Entry](img/validEntry.png "Valid Entry") | ![Passed Validation](img/passedValidation.png "Passed Validation") 

 **Screenshot of Display Data:** 

![Display Data](img/displayData.png "Display Data")  

| Screenshot of Modify Form: | Screenshot of Modified Data: 
| :-|:-|:-
![Modify Form](img/modifyForm.png "Modify Form") | ![Modified Data](img/modifiedData.png "Modified Data") 

**Screenshot of Delete Warning:**

![Delete Warning](img/deleteWarning.png "Delete Warning")

#### Screenshot of Associated Database Changes (Select, Insert, Update, Delete):
| Select (Original Data): |  
| :-|:-|:-
![Data Pic 1](img/datapic1.png "Data Pic 1") 

| Update: |  
| :-|:-|:-
![Data Pic 2](img/datapic2.png "Data Pic 2") 

| Insert: |  
| :-|:-|:-
![Insert Pic 1](img/sqlModifiedData.png "Insert Pic 1")

| Delete: |  
| :-|:-|:-
![Deleted Entry](img/deletedSQLEntry.png "Deleted Entry")























