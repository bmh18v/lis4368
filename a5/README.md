> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Assignment 5 Requirements:

*Four Parts:*

1. Include server-side validation from A4
2. Compile all class and servlet files
3. Skillsets 13-15

#### README.md file should include the following items:

* Screenshot of valid user form entry
* Screenshot of successful data entry
* Screenshot of customer table with entry in command line
* Screenshot of skillsets 13-15

Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4)


#### Assignment Screenshots:

#### *Screenshot of Valid User Form Entry:*

![A5 User Entry](img/A5_validData.png "Valid Data")

#### *Screenshot of Successful Data Entry:*

![Data Entry](img/passedValidation.png "Data Entry")

#### *Database Entry in Command Line:*

![CMD Data Entry](img/mysqlUpdated.png "CMD Data Entry")

#### Skill Sets 13-15:

| Skillset 13: Java Number App Swap  | Skillset 14: Java Largest of Three Numbers 
| :-|:-|:-
![Skill Set 13](img/Skillsets13NS.png "Number Swap") | ![Skill Set 14](img/Skillsets14LO3.png "Largest of Three App")  

| Skillset 15: Java Simple Calculator Using Methods
| :-|:-|:-
![Skill Set 15](img/Skillsets15CM.png "Calculator Methods App")























