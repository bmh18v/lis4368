> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Assignment 4 Requirements:

*Three Parts:*

1. Create server side validation page
2. Test validations
3. Skill sets 10-12

#### README.md file should include the following items:

* Screenshots of failed and passed verifications
* Screenshots of Skill Sets 10-12

Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/customerform.jsp?assign_num=a4)


#### Assignment Screenshots:

#### *Screenshot of Failed Verification:*

![A4 Failed](img/failed_verification.png "Failed Verification")

#### *Screenshot of Passed Verification:*

![A4 Passed](img/passed_verification.png "Passed Verification")

#### Skill Sets 10-12:

| Skillset 10: Java Count Characters  | Skillset 11: Java File Write/Read Count Words 
| :-|:-|:-
![Skill Set 10](img/skillset10.png "Count Characters") | ![Skill Set 11](img/skillset11.png "File Write/Read") 

| Skillset 12: ASCII App Part 1 | ASCII App Part 2
| :-|:-|:-
![ASCII Part 1](img/skillset12p1.png "ASCII Part 1") | ![ASCII Part 2](img/skillset12p2.png "ASCII Part 2") |























