> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Assignment 1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello
* Screenshot of running http://localhost:9999
* Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/)
* Git commands with short descriptions
* Bitbucket repo links a) this assignment and b) the completed tutorial above (bitbucketstationlocations). 

> #### Git commands w/short descriptions:

1. git init - creates a new repo
2. git status - displays the state of the working directory and the staging area. 
3. git add - adds a change in the working directory to the staging area
4. git commit - commits the staged snapshot to the project history
5. git push - publish new local commits on a remote server
6. git pull - used to fetch and download content from a remote repo and immediately update the local repo to match that content.
7. git merge - merge a different branch into your active branch


#### Assignment Screenshots:

*Screenshot of running java Hello:*

![Screenshot of java Hello](img/javaHello.png)

*Screenshot of running http://localhost:9999*:

![Local Host Running](img/localHost.png)

*Screenshot of A1 Requirements on Local Host Website*:
![A1 Local Website](img/localWebsite.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bmh18v/bitbucketstationlocations/ "Bitbucket Station Locations")


