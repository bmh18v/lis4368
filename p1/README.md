> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Project 1 Requirements:

*Four Parts:*

1. Modify tags in index.jsp
2. Change title, navigation links, and header tags to appropriate format
3. Add form controls to match attributes of customer entity
4. Use min/max jQuery validation
5. Use *regexp* to allow only certain characters for input boxes
	

#### README.md file should include the following items:

* Screenshot of Main/Splash Page
* Screenshot of Failed Data Validation
* Screenshot of Passed Data Validation

Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/p1/index.jsp)


#### Assignment Screenshots:

| Screenshot of Main Splash 1: | Screenshot of Main Splash 2: | Screenshot of Main Splash 3:
| :-|:-|:-
![Main Splash 1](img/MainSplash.png "Main Splash 1") | ![Main Splash 2](img/MainSplash2.png "Main Splash 2") | ![Main Splash 3](img/MainSplah3.png "Main Splash 3")

 Screenshot of Failed Data Verification: 

![Failed Verification](img/nonValidated.png "Failed Verification")  

Screenshot of *Invalid* Data Verification:

![Invalid Verification](img/invalidValue.png "Invalid Verification")

Screenshot of Passed Verification:

![Passed Verification](img/passValidation.png "Passed Verification")

















