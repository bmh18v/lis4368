> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368 - Advanced Web Applications Development

## Bryan Humphries

### Assignment 3 Requirements:

*Three Parts:*

1. Create Entity Relationship Diagram (ERD)
2. Include 10 Records of Data in Each Table
3. Include Image of A3 ERD

#### README.md file should include the following items:

* Completed ERD Screenshot
* Screenshot of A3 index.jsp
* Links to both [A3 MWB File](docs/a3.mwb "A3 ERD in .mwb") and [A3 SQL File](docs/a3.sql "A3 SQL Script")

Link to local lis4368 web app: [Local Web App](http://localhost:9999/lis4368/a3/index.jsp)


#### Assignment Screenshots:

*Screenshot of A3 ERD:*

![A3 ERD](img/a3_erd.png "A3 ERD")

*Screenshot of A3 index.jsp*

![A3 Index.jsp](img/a3_indexjsp.png "A3 Index.jsp")

















